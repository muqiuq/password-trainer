﻿using PasswordTrainer.Model;
using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordTrainer
{
    public partial class EditPasswordForm : Form
    {
        private PasswordTrainerFile passwordTrainerFile;
        private PasswordEntry passwordEntry;
        private bool isNew;

        public EditPasswordForm(PasswordTrainerFile passwordTrainerFile, bool isNew = true, PasswordEntry passwordEntry = null)
        {
            this.passwordTrainerFile = passwordTrainerFile;

            this.passwordEntry = passwordEntry;

            this.isNew = isNew;

            InitializeComponent();

            if(!isNew)
            {
                buttonSave.Text = "Save";
                textBoxName.Text = passwordEntry.Name;
                checkBoxEnabled.Checked = passwordEntry.Enabled;
            }
            buttonDelete.Visible = !isNew;
            checkBoxEnabled.Visible = !isNew;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if(textBoxName.Text.Trim() == "")
            {
                labelErrorText.Text = "Please enter name";
                return;
            }
            if(isNew && maskedTextBoxPw1.Text.Trim() == "")
            {
                labelErrorText.Text = "Please enter password.";
                return;
            }
            if(maskedTextBoxPw1.Text.Trim() != "" & maskedTextBoxPw1.Text != maskedTextBoxPw2.Text)
            {
                labelErrorText.Text = "Passwords do not match";
                return;
            }

            var salt = passwordEntry == null ? Helper.RandomString(Global.SALT_LENGTH) : passwordEntry.Salt;
            var hashedPassword = Helper.HashPassword(passwordTrainerFile.MasterSalt, 
                salt, maskedTextBoxPw1.Text);

            if(isNew)
            {
                passwordEntry = new PasswordEntry()
                {
                    Name = textBoxName.Text.Trim(),
                    Salt = salt,
                    HashedPassword = hashedPassword
                };
                passwordTrainerFile.PasswordEntries.Add(passwordEntry);
            }
            else
            {
                passwordEntry.Name = textBoxName.Text.Trim();
                passwordEntry.Enabled = checkBoxEnabled.Checked;
                if(maskedTextBoxPw1.Text.Trim() != "")
                {
                    passwordEntry.HashedPassword = hashedPassword;
                }
            }

            this.Close();
        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            labelErrorText.Text = "";
        }

        private void maskedTextBoxPw2_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                buttonSave_Click(sender, null);
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure?", "Confirm action", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if(result == DialogResult.Yes)
            {
                passwordTrainerFile.PasswordEntries.Remove(passwordEntry);
                Close();
            }
        }
    }
}
