﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PasswordTrainer
{
    public static class Helper
    {

        public static string HashPassword(string masterSalt, string salt, string password)
        {
            var sha256 = SHA256.Create();

            return Convert.ToBase64String(sha256.ComputeHash(Encoding.UTF8.GetBytes($"{masterSalt}-{salt}-{password}")));
        }

        public static string RandomString(int length)
        {
            const string valid = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];

                while (length-- > 0)
                {
                    rng.GetBytes(uintBuffer);
                    uint num = BitConverter.ToUInt32(uintBuffer, 0);
                    res.Append(valid[(int)(num % (uint)valid.Length)]);
                }
            }

            return res.ToString();
        }
    }
}
