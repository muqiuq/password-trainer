﻿using PasswordTrainer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PasswordTrainer
{
    public class MasterController
    {

        private static MasterController defaultMasterController;

        public static MasterController Default { get
            {
                if(defaultMasterController == null)
                {
                    throw new ArgumentNullException("Please call MasterController.Init() first.");
                }
                return defaultMasterController;
            } }

        internal static void Init()
        {
            if (defaultMasterController == null)
            {
                defaultMasterController = new MasterController();
            }
        }



        public PasswordTrainerFile PasswordTrainerFile;
        private string passwordTrainerFilePath;
        private JsonDocumentOptions jsonDocumentOptions;
        private JsonSerializerOptions jsonSerializerOptions;

        public MasterController()
        {
            passwordTrainerFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "PasswordTrainer.json");

            jsonDocumentOptions = new JsonDocumentOptions
            {
                AllowTrailingCommas = true
            };

            jsonSerializerOptions = new JsonSerializerOptions()
            {
                WriteIndented = true,
            };
        }

        public bool IsFileLoaded { get
            {
                return PasswordTrainerFile != null;
            } 
        }
        

        public void CheckIfDefaultFilesExistsAndCreate()
        {
            if (!File.Exists(passwordTrainerFilePath))
            {
                PasswordTrainerFile = new PasswordTrainerFile()
                {
                    Name = $"{Environment.MachineName}-{Environment.UserName}",
                    MasterSalt = Helper.RandomString(Global.SALT_LENGTH),
                    Version = Global.FILE_VERSION_MAX
                };
                SaveDefault();
            }
        }

        public string HashPassword(PasswordEntry entry, string password)
        {
            return Helper.HashPassword(PasswordTrainerFile.MasterSalt, entry.Salt, password);
        }

        public void SaveDefault()
        {
            Save(passwordTrainerFilePath);
        }

        public void Save(string filename)
        {
            using FileStream fs = File.Create(filename);
            using var writer = new Utf8JsonWriter(fs);
            JsonSerializer.Serialize(writer, PasswordTrainerFile, options: jsonSerializerOptions);
        }

        internal void CopyDefaultFileTo(string fileName)
        {
            File.Copy(passwordTrainerFilePath, fileName);
        }

        internal void LoadDefaultFile()
        {
            LoadFile(passwordTrainerFilePath);
        }

        internal void LoadFile(string fileName)
        {
            var json = File.ReadAllText(fileName);
            var tempPasswordTrainerFile = JsonSerializer.Deserialize<PasswordTrainerFile>(json, jsonSerializerOptions);
            if (tempPasswordTrainerFile.Version == -1)
            {
                throw new InvalidVersionException($"Version not present in Json");
            }
            if (tempPasswordTrainerFile.Version > Global.FILE_VERSION_MAX)
            {
                throw new InvalidVersionException($"Fileversion {tempPasswordTrainerFile.Version} is not supported. (Only up to {Global.FILE_VERSION_MAX})");
            }
            PasswordTrainerFile = tempPasswordTrainerFile;
        }
    }
}
