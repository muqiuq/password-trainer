﻿using PasswordTrainer.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordTrainer
{
    public partial class Form1 : Form
    {

        PasswordEntry currentEntry;

        int numberOfWrongTries = 0;

        public Form1()
        {
            InitializeComponent();

            MasterController.Init();

            MasterController.Default.CheckIfDefaultFilesExistsAndCreate();

            LoadPasswortTrainerFile();
            
            changeCurrentEntryRandomly();
        }

        private void LoadPasswortTrainerFile(string filename = null)
        {
            try
            {
                if (filename == null) MasterController.Default.LoadDefaultFile();
                else MasterController.Default.LoadFile(filename);
            }
            catch (Exception e)
            {
                if(e is InvalidVersionException || e is JsonException)
                MessageBox.Show($"Error while Loading file. \n({e.Message})", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            toolStripStatusLabel1.Text = MasterController.Default.PasswordTrainerFile.Name ??= "";
        }

        private void changeCurrentEntryRandomly() 
        {
            if (!MasterController.Default.IsFileLoaded) return;
            
            var newCurrentEntry = MasterController.Default.PasswordTrainerFile.GetRandomEntry(currentEntry);

            if(newCurrentEntry != null)
            {
                currentEntry = newCurrentEntry;
                labelPasswortType.Text = currentEntry.Name;
                maskedTextBoxPassword.Enabled = true;
            }
        }

        private void addPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new EditPasswordForm(MasterController.Default.PasswordTrainerFile)).ShowDialog(this);

            MasterController.Default.SaveDefault();

            changeCurrentEntryRandomly();
        }

        private void entriesToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (!MasterController.Default.IsFileLoaded) return;
            entriesToolStripMenuItem.DropDownItems.Clear();
            var addMenuItem = new ToolStripMenuItem($"Add");
            addMenuItem.Click += addPasswordToolStripMenuItem_Click;
            entriesToolStripMenuItem.DropDownItems.Add(addMenuItem);
            MasterController.Default.PasswordTrainerFile.PasswordEntries.ForEach(i =>
                {
                    var tsmi = new ToolStripMenuItem($" - {i.Name}");
                    tsmi.Click += (object sender, EventArgs e) => {
                        (new EditPasswordForm(MasterController.Default.PasswordTrainerFile, isNew: false, i)).ShowDialog(this);
                        MasterController.Default.SaveDefault();
                    };
                    entriesToolStripMenuItem.DropDownItems.Add(tsmi);
                }
            );

        }

        private void maskedTextBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if(maskedTextBoxPassword.Text == "")
                {
                    changeCurrentEntryRandomly();
                    return;
                }

                var hashedEnteredPassword = MasterController.Default.HashPassword(currentEntry, maskedTextBoxPassword.Text);

                if (currentEntry.HashedPassword == hashedEnteredPassword)
                {
                    labelResult.ForeColor = Color.Green;
                    labelResult.Text = "Corrent!😃";
                    changeCurrentEntryRandomly();
                }
                else
                {
                    labelResult.ForeColor = Color.Red;
                    labelResult.Text = "Wrong!😔";
                    numberOfWrongTries++;
                }
                if(numberOfWrongTries > 3)
                {
                    changeCurrentEntryRandomly();
                    numberOfWrongTries = 0;
                }
                maskedTextBoxPassword.Text = "";
            }
            else
            {
                if (e.KeyCode == Keys.Delete)
                {
                    maskedTextBoxPassword.Text = "";
                }
                labelResult.Text = "";
            }
        }

        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = saveFileDialogBackup.ShowDialog(this);

            if(result == DialogResult.OK)
            {
                MasterController.Default.CopyDefaultFileTo(saveFileDialogBackup.FileName);
            }

        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = openFileDialogImport.ShowDialog(this);

            if(result == DialogResult.OK)
            {
                LoadPasswortTrainerFile(openFileDialogImport.FileName);
                MasterController.Default.SaveDefault();
                changeCurrentEntryRandomly();
            }
        }
    }
}
