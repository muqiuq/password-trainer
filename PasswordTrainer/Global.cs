﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordTrainer
{
    public class Global
    {
        // For Master and specific salt
        public static readonly int SALT_LENGTH = 64;

        public static readonly int FILE_VERSION_MAX = 1;
    }
}
