﻿namespace PasswordTrainer.Model
{
    public class PasswordEntry
    {

        public string Name { get; set; }

        public string HashedPassword { get; set; }

        public string Salt { get; set; }

        public bool Enabled { get; set; } = true;

        public override string ToString()
        {
            return Name;
        }

    }
}