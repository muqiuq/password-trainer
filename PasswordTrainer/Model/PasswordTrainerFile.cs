﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordTrainer.Model
{
    public class PasswordTrainerFile
    {

        public string Name { get; set; }
     
        public string MasterSalt { get; set; }

        public int Version { get; set; } = -1;

        public List<PasswordEntry> PasswordEntries { get; set; } = new List<PasswordEntry>();

        public List<PasswordEntry> EnabledEntries { 
            get
            {
                return PasswordEntries.Where(i => i.Enabled == true).ToList();
            } 
        }

        internal PasswordEntry GetRandomEntry(PasswordEntry currentEntry = null)
        {
            Random r = new Random();
            int i = 0;
            PasswordEntry randomEntry = null;
            if (EnabledEntries.Count == 0) return null;
            while (i < 10)
            {
                randomEntry = EnabledEntries[r.Next(0, EnabledEntries.Count)];
                if (currentEntry == null || currentEntry != randomEntry) return randomEntry;
                i++;
            }
            return randomEntry;
        }
    }
}
