﻿
namespace PasswordTrainer
{
    partial class EditPasswordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.maskedTextBoxPw1 = new System.Windows.Forms.MaskedTextBox();
            this.labelPw1 = new System.Windows.Forms.Label();
            this.labelPw2 = new System.Windows.Forms.Label();
            this.maskedTextBoxPw2 = new System.Windows.Forms.MaskedTextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelErrorText = new System.Windows.Forms.Label();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.checkBoxEnabled = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelName.Location = new System.Drawing.Point(12, 14);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(55, 21);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Name:";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxName
            // 
            this.textBoxName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxName.Location = new System.Drawing.Point(128, 11);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(300, 29);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // maskedTextBoxPw1
            // 
            this.maskedTextBoxPw1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.maskedTextBoxPw1.Location = new System.Drawing.Point(128, 61);
            this.maskedTextBoxPw1.Name = "maskedTextBoxPw1";
            this.maskedTextBoxPw1.PasswordChar = '#';
            this.maskedTextBoxPw1.Size = new System.Drawing.Size(300, 29);
            this.maskedTextBoxPw1.TabIndex = 2;
            this.maskedTextBoxPw1.Enter += new System.EventHandler(this.textBox_Enter);
            // 
            // labelPw1
            // 
            this.labelPw1.AutoSize = true;
            this.labelPw1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelPw1.Location = new System.Drawing.Point(12, 64);
            this.labelPw1.Name = "labelPw1";
            this.labelPw1.Size = new System.Drawing.Size(79, 21);
            this.labelPw1.TabIndex = 3;
            this.labelPw1.Text = "Password:";
            this.labelPw1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPw2
            // 
            this.labelPw2.AutoSize = true;
            this.labelPw2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelPw2.Location = new System.Drawing.Point(12, 98);
            this.labelPw2.Name = "labelPw2";
            this.labelPw2.Size = new System.Drawing.Size(61, 21);
            this.labelPw2.TabIndex = 5;
            this.labelPw2.Text = "Repeat:";
            this.labelPw2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // maskedTextBoxPw2
            // 
            this.maskedTextBoxPw2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.maskedTextBoxPw2.Location = new System.Drawing.Point(128, 95);
            this.maskedTextBoxPw2.Name = "maskedTextBoxPw2";
            this.maskedTextBoxPw2.PasswordChar = '#';
            this.maskedTextBoxPw2.Size = new System.Drawing.Size(300, 29);
            this.maskedTextBoxPw2.TabIndex = 4;
            this.maskedTextBoxPw2.Enter += new System.EventHandler(this.textBox_Enter);
            this.maskedTextBoxPw2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.maskedTextBoxPw2_KeyDown);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(352, 135);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 6;
            this.buttonSave.Text = "Add";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelErrorText
            // 
            this.labelErrorText.AutoSize = true;
            this.labelErrorText.ForeColor = System.Drawing.Color.Red;
            this.labelErrorText.Location = new System.Drawing.Point(13, 135);
            this.labelErrorText.Name = "labelErrorText";
            this.labelErrorText.Size = new System.Drawing.Size(0, 15);
            this.labelErrorText.TabIndex = 7;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(239, 135);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 8;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // checkBoxEnabled
            // 
            this.checkBoxEnabled.AutoSize = true;
            this.checkBoxEnabled.Location = new System.Drawing.Point(19, 138);
            this.checkBoxEnabled.Name = "checkBoxEnabled";
            this.checkBoxEnabled.Size = new System.Drawing.Size(68, 19);
            this.checkBoxEnabled.TabIndex = 9;
            this.checkBoxEnabled.Text = "Enabled";
            this.checkBoxEnabled.UseVisualStyleBackColor = true;
            // 
            // EditPasswordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 174);
            this.Controls.Add(this.checkBoxEnabled);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.labelErrorText);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.labelPw2);
            this.Controls.Add(this.maskedTextBoxPw2);
            this.Controls.Add(this.labelPw1);
            this.Controls.Add(this.maskedTextBoxPw1);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditPasswordForm";
            this.Text = "Password entry";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPw1;
        private System.Windows.Forms.Label labelPw1;
        private System.Windows.Forms.Label labelPw2;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPw2;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelErrorText;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.CheckBox checkBoxEnabled;
    }
}